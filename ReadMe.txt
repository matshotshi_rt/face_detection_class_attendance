Requirement
node 12
vcode IDE

Install steps
1 npm install -g @angular/cli
2 ng new my-dream-app
3 cd my-dream-app
4 ng serve

5 ng add @angular/material  

6 npm i highcharts-angular --save 

7 npm i highcharts --save 

8 npm i @angular/flex-layout @angular/cdk --save

Project layout
1. Create the first layout [ ng g c layouts/default ] its module [ng g m layouts/default]
2. Create a component for the Dashboard page [ng g c modules/dashboard] ng g m modules/dashboard
3. Use the first layout as a Parent Route component in app-routing.module and the Dashboard page as child component

Shared Modules
Create a share module for the header, sidebar and footer for better code organisation
1. Create header, footer, sidebar components
2. Create a shared module for it
3. Update the module and those components in the declaration and exports sections



